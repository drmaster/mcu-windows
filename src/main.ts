import { createApp } from 'vue'
import { createPinia } from 'pinia'

import { Quasar, Dialog, Notify } from 'quasar'
import quasarLang from 'quasar/lang/zh-TW'

// Import icon libraries
import '@quasar/extras/roboto-font/roboto-font.css'
import '@quasar/extras/material-icons/material-icons.css'

// Import Quasar css
import 'quasar/src/css/index.sass'
import App from './App.vue'

// 自訂樣式
import './style/global.sass'
import './style/animate.sass'

import './index.css'


createApp(App)
  .use(Quasar, {
    plugins: {
      Dialog, Notify
    },
    lang: quasarLang,
  })
  .use(createPinia())
  .mount('#app')

