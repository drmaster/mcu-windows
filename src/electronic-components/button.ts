import EventEmitter2, { Listener, OnOptions } from "eventemitter2";
import { debounce, findLast } from "lodash";
import { PinMode } from "../common/firmata/firmata-utils";
import { DigitalData, Pin } from "../common/firmata/response-define";
import { PortTransceiver } from "../common/port-transceiver";
import { delay, getBitWithNumber } from "../common/utils";

export interface ConstructorParams {
  pin: Pin;
  transceiver: PortTransceiver;
  mode: PinMode;
  debounceMillisecond?: number;
}

export enum EventName {
  RISING = 'rising',
  FALLING = 'falling',
  TOGGLE = 'toggle',
}

/** 
 * 基本按鈕
 * 
 * 繼承 EventEmitter2 功能，支援數位輸入、上拉輸入
 */
export class Button extends EventEmitter2 {
  /** 指定腳位  */
  private pin: Pin;
  /** 腳位 Port 號 */
  private portNumber: number;
  /** 腳位模式 */
  private mode: number;
  /** COM Port 收發器 */
  private portTransceiver: PortTransceiver;
  /** 訊號回報監聽器 */
  private listener?: Listener;
  /** 目前數位訊號 */
  value = false;

  private debounce: {
    processEvent: ReturnType<typeof debounce>
  }

  constructor(params: ConstructorParams) {
    super();

    const {
      pin,
      transceiver,
      mode = PinMode.DIGITAL_INPUT,
      debounceMillisecond = 10,
    } = params;

    if (!pin) throw new Error(`pin 必填`);
    if (!transceiver) throw new Error(`transceiver 必填`);
    if (![PinMode.DIGITAL_INPUT, PinMode.INPUT_PULLUP].includes(mode)) {
      throw new Error(`不支援指定的腳位模式 : ${mode}`);
    }

    this.pin = pin;
    this.portNumber = (pin.number / 8) | 0;
    this.mode = mode;
    this.portTransceiver = transceiver;

    this.debounce = {
      processEvent: debounce((params: boolean) => {
        this.processEvent(params)
      }, debounceMillisecond),
    }

    this.init();
  }

  async init() {
    this.portTransceiver.addCmd('set-mode', {
      pin: this.pin.number,
      mode: this.mode,
    });

    // 延遲一下再監聽數值，忽略 set-mode 初始化的數值變化
    await delay(500);

    this.listener = this.portTransceiver.on(
      'digital-data', (data) => this.handleData(data),
      { objectify: true }
    ) as Listener;
  }
  destroy() {
    // 銷毀所有監聽器，以免 Memory Leak
    this.listener?.off();
    this.removeAllListeners();
  }

  private handleData(data: DigitalData[]) {
    // Port 不同表示不是對應腳位範圍
    const target = findLast(data, ({ port }) => this.portNumber === port);
    if (!target) return;

    const { value } = target;
    const bitIndex = this.pin.number % 8;

    const bitValue = getBitWithNumber(value, bitIndex);
    this.debounce.processEvent(bitValue);
  }

  /** 依照數位訊號判斷按鈕事件
   * - rising：上緣，表示按下按鈕
   * - falling：下緣，表示放開按鈕
   * - toggle：訊號切換，放開、按下都觸發
   */
  private processEvent(value: boolean) {
    let correctionValue = value;

    // 若為上拉輸入，則自動反向
    if (this.mode === PinMode.INPUT_PULLUP) {
      correctionValue = !correctionValue;
    }

    // 訊號沒變，不做任何處理
    if (this.value === correctionValue) return;

    if (correctionValue) {
      this.emit(EventName.RISING);
    }
    if (!correctionValue) {
      this.emit(EventName.FALLING);
    }

    this.emit(EventName.TOGGLE, correctionValue);
    this.value = correctionValue;
  }
}

type ListenerOption = boolean | OnOptions;
export interface Button {
  on(event: `${EventName.RISING}`, listener: () => void, options?: ListenerOption): this | Listener;
  on(event: `${EventName.FALLING}`, listener: () => void, options?: ListenerOption): this | Listener;
  on(event: `${EventName.TOGGLE}`, listener: (value: boolean) => void, options?: ListenerOption): this | Listener;

  once(event: `${EventName.RISING}`, listener: () => void, options?: ListenerOption): this | Listener;
  once(event: `${EventName.FALLING}`, listener: () => void, options?: ListenerOption): this | Listener;
  once(event: `${EventName.TOGGLE}`, listener: (value: boolean) => void, options?: ListenerOption): this | Listener;
}