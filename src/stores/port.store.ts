import { defineStore } from 'pinia';
import { PortTransceiver } from '../common/port-transceiver';

interface State {
  port?: SerialPort;
  transceiver?: PortTransceiver;
}

export const usePortStore = defineStore('port', {
  state: (): State => ({
    port: undefined,
    transceiver: undefined,
  }),
  actions: {
    /** 設定 Port，將 SerialPort 物件儲存至 store 中 */
    setPort(port: SerialPort) {
      this.port = port;

      // 若 transceiver 已經建立，則關閉
      this.transceiver?.stop();

      // 建立 PortTransceiver 物件
      this.transceiver = new PortTransceiver(port);
      this.transceiver.start();
    },
  }
})