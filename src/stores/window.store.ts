import { nanoid } from 'nanoid';
import { defineStore } from 'pinia';
import { defineComponent, markRaw } from 'vue';
import { Pin } from '../common/firmata/response-define';

import WindowDigitalIo from '../components/window-digital-io.vue';
import WindowAnalogInput from '../components/window-analog-input.vue';
import WindowAppCatVsDog from '../components/window-app-cat-vs-dog/window-app-cat-vs-dog.vue';

/** 列舉並儲存視窗組件 */
const windowComponentMap = {
  'window-digital-io': markRaw(WindowDigitalIo),
  'window-analog-input': markRaw(WindowAnalogInput),
  'window-app-cat-vs-dog': markRaw(WindowAppCatVsDog),
}
/** 可用視窗名稱 */
export type WindowName = keyof typeof windowComponentMap;

interface Window {
  /** 視窗組件：表示此視窗具體是何種視窗 */
  component: ReturnType<typeof defineComponent>;
  /** 視窗 ID：每個視窗的唯一 ID，用來識別視窗 */
  id: string;
  /** 聚焦時間：用來判斷視窗重疊關係 */
  focusAt: number;
  /** 占用腳位 */
  occupiedPins: Pin[];
}

interface State {
  map: Map<string, Window>;
  focusedId?: string;
}

export const useWindowStore = defineStore('window', {
  state: (): State => ({
    map: new Map(),
    focusedId: undefined,
  }),
  actions: {
    /** 新增視窗
     * @param name 組件名稱
     */
    add(name: WindowName) {
      /** 使用 nanoid 生成 ID */
      const id = nanoid();

      this.map.set(id, {
        component: windowComponentMap[name],
        id,
        focusAt: Date.now(),
        occupiedPins: [],
      });
    },

    /** 刪除視窗 */
    remove(id: string) {
      this.map.delete(id);
    },

    /** focus 指定視窗 */
    async setFocus(id: string) {
      this.focusedId = id;

      const target = this.map.get(id);
      if (!target) {
        return Promise.reject(`視窗不存在`);
      }

      target.focusAt = Date.now();
    },

    /** 新增 Window 占用腳位 */
    async addOccupiedPin(id: string, pin: Pin) {
      const windows = [...this.map.values()];

      const target = windows.find((window) => window.id === id);
      if (!target) {
        return Promise.reject(`window 不存在`);
      }

      target.occupiedPins.push(pin);
    },

    /** 移除 Window 占用腳位 */
    async deleteOccupiedPin(id: string, pin: Pin) {
      const windows = [...this.map.values()];

      const target = windows.find((window) => window.id === id);
      if (!target) {
        return Promise.reject(`window 不存在`);
      }

      const targetPinIndex = target.occupiedPins.findIndex(({ number }) =>
        number === pin.number
      );
      if (targetPinIndex < 0) return;

      target.occupiedPins.splice(targetPinIndex, 1);
    },
  },
  getters: {
    /** 視窗 ID 對應 z-index */
    zIndexMap(state) {
      const windows = [...state.map].map(([key, value]) => value);

      windows.sort((a, b) => a.focusAt > b.focusAt ? 1 : -1);

      return windows.reduce((map, window, index) =>
        map.set(window.id, index),
        new Map<string, number>()
      );
    },

    /** 所有被占用腳位，包含占用 window 的 ID */
    occupiedPins(state) {
      const windows = [...state.map.values()];

      // 找出有占用腳位的 window
      const occupiedPinWindows = windows.filter(({ occupiedPins }) =>
        occupiedPins.length !== 0
      );

      const occupiedPins = occupiedPinWindows.reduce<{
        pin: Pin;
        ownedWindowId: string;
      }[]>((acc, window) => {
        window.occupiedPins.forEach((pin) => {
          acc.push({
            pin,
            ownedWindowId: window.id,
          });
        });

        return acc;
      }, []);

      return occupiedPins;
    },
  }
})
