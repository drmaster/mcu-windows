import { defineStore } from 'pinia';
import { AnalogPinMap, FirmataInfo, Pin } from '../common/firmata/response-define';

interface State {
  version?: string;
  firmwareName?: string;
  pins: Pin[];
  analogPinMap?: AnalogPinMap;
}

export const useBoardStore = defineStore('board', {
  state: (): State => ({
    version: undefined,
    firmwareName: undefined,
    pins: [],
    analogPinMap: undefined,
  }),
  actions: {
    setInfo({ version, firmwareName }: FirmataInfo) {
      this.$patch({
        version,
        firmwareName
      });
    },
    setPins(pins: Pin[]) {
      this.$patch({
        pins
      });
    },
    setAnalogPinMap(analogPinMap: AnalogPinMap) {
      this.$patch({
        analogPinMap
      });
    },
  }
})