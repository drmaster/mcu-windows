import { chunk } from "lodash";
import { arraySplit, matchFeature, significantBytesToNumber } from "../utils";

/** 回應 Key 種類 */
export enum ResponseKey {
  FIRMWARE_NAME = 'firmware-name',
  CAPABILITY = 'capability',
  ANALOG_MAPPING = 'analog-mapping',
  DIGITAL_MESSAGE = 'digital-message',
  ANALOG_MESSAGE = 'analog-message',
}

/** 事件名稱種類 */
export enum EventName {
  INFO = 'info',
  DIGITAL_DATA = 'digital-data',
  ANALOG_DATA = 'analog-data',
}

/** 回應基本定義 */
interface ResponseDefine<T extends `${ResponseKey}`, K extends `${EventName}`, U> {
  /** 回應資料的 key */
  key: T;
  /** 此資料對應觸發的 event 名稱 */
  eventName: K;
  /** 用來判斷回應資料是否符合 */
  match: (res: number[]) => boolean;
  /** 從 byte 中取得資料 */
  getData: (res: number[]) => U;
}

/** Firmata 資訊 */
export interface FirmataInfo {
  /** 版本號 */
  version: string;
  /** 韌體名稱 */
  firmwareName: string;
}
/** Firmata 腳位資料 */
export interface FirmataPins {
  /** 版本號 */
  pins: Pin[];
}
/** Firmata 類比腳位映射表 */
export interface FirmataAnalogPinMap {
  /** 版本號 */
  analogPinMap: AnalogPinMap;
}

/** 腳位 */
export interface Pin {
  /** 腳位編號 */
  number: number;
  /** 腳位能力 */
  capabilities: Capability[];
}
/** 功能 */
export interface Capability {
  /** 支援模式 */
  mode: number;
  /** 解析度 */
  resolution: number;
}
/** 類比腳位映射表 */
export interface AnalogPinMap {
  /** 原腳位編號對應映射後的編號 */
  [pinNumber: string]: number;
}
/** 數位訊號資料 */
export interface DigitalData {
  /** port 編號 */
  port: number;
  /** 以二進位形式儲存整個 Port 腳位狀態 */
  value: number;
}
/** 類比訊號資料 */
export interface AnalogData {
  /** 類比編號，實際腳位編號需要使用 AnalogPinMap 查詢 */
  analogNumber: number;
  /** ADC 轉換後的數值 */
  value: number;
}

export type FirmataData = ReturnType<FirmataResponse['getData']>;

type ResponseFirmwareName = ResponseDefine<'firmware-name', 'info', FirmataInfo>;
type ResponseCapability = ResponseDefine<'capability', 'info', FirmataPins>;
type ResponseAnalogMapping = ResponseDefine<'analog-mapping', 'info', FirmataAnalogPinMap>;
type ResponseDigitalData = ResponseDefine<'digital-message', 'digital-data', DigitalData[]>;
type ResponseAnalogData = ResponseDefine<'analog-message', 'analog-data', AnalogData[]>;

export type FirmataResponse = ResponseFirmwareName | ResponseCapability
  | ResponseAnalogMapping | ResponseDigitalData | ResponseAnalogData;

/** 回應定義清單 */
export const responses: FirmataResponse[] = [
  // firmware-name: 韌體名稱與版本
  {
    key: 'firmware-name',
    eventName: 'info',
    match(res) {
      // 回應開頭一定為 F0 79
      const featureBytes = [0xF0, 0x79];
      return matchFeature(res, featureBytes);
    },

    getData(res) {
      // 取得特徵起點
      const index = res.lastIndexOf(0x79);

      // 版本號
      const major = res[index + 1];
      const minor = res[index + 2];

      // 取得剩下的資料
      const nameBytes = res.slice(index + 3, -1);

      /** 由於 MSB 都是 0
       * 所以去除 0 之後，將剩下的 byte 都轉為字元後合併
       * 最後的結果就會是完整的名稱
       */
      const firmwareName = nameBytes
        .filter((byte) => byte !== 0)
        .map((byte) => String.fromCharCode(byte))
        .join('');

      return {
        version: `${major}.${minor}`,
        firmwareName
      }
    },
  },
  // capability: 腳位與功能
  {
    key: 'capability',
    eventName: 'info',
    match(res) {
      const featureBytes = [0xF0, 0x6C];
      return matchFeature(res, featureBytes);
    },

    getData(res) {
      // 去除起始值、命令代號、結束值
      const values = res.filter((byte) =>
        ![0xF0, 0x6C, 0xF7].includes(byte)
      );

      // 根據分隔符號 0x7F 分割
      const pinParts = arraySplit(values, 0x7F);

      // 依序解析所有 pin
      const pins: Pin[] = pinParts.map((pinPart, index) => {
        // 每 2 個數值一組
        const modeParts = chunk(pinPart, 2);

        // 第一個數值為模式，第二個數值為解析度
        const capabilities: Capability[] = modeParts.map((modePart) => {
          const [mode, resolution] = modePart;
          return {
            mode, resolution
          }
        });

        return {
          number: index,
          capabilities,
        }
      });

      return { pins };
    },
  },

  // analog-mapping: 類比腳位映射表
  {
    key: 'analog-mapping',
    eventName: 'info',
    match(res) {
      const featureBytes = [0xF0, 0x6A];
      return matchFeature(res, featureBytes);
    },
    getData(values) {
      // 找到 6A 之 Index，從這裡開始往後找
      const index = values.findIndex((byte) => byte === 0x6A);

      const bytes = values.slice(index + 1, -1);

      const analogPinMap = bytes.reduce<AnalogPinMap>((map, byte, index) => {
        // 127（0x7F）表示不支援類比功能
        if (byte !== 127) {
          map[`${index}`] = byte;
        }

        return map;
      }, {});

      return { analogPinMap };
    },
  },

  // digital-message: 數位訊息回應
  {
    key: 'digital-message',
    eventName: 'digital-data',
    match(res) {
      const hasCmd = res.some((byte) => byte >= 0x90 && byte <= 0x9F);
      return hasCmd;
    },
    getData(values) {
      // 取得所有特徵點位置
      const indexes = values.reduce<number[]>((acc, byte, index) => {
        if (byte >= 0x90 && byte <= 0x9F) {
          acc.push(index);
        }
        return acc;
      }, []);

      const responses = indexes.reduce<DigitalData[]>((acc, index) => {
        const bytes = values.slice(index + 1, index + 3);

        const port = values[index] - 0x90;
        const value = significantBytesToNumber(bytes);

        acc.push({
          port, value,
        });
        return acc;
      }, []);

      return responses;
    },
  },

  // analog-message: 類比訊息回應
  {
    key: 'analog-message',
    eventName: 'analog-data',
    match(res) {
      return res.some((byte) => byte >= 0xE0 && byte <= 0xEF);
    },
    getData(values) {
      // 取得所有特徵點位置
      const indexes = values.reduce<number[]>((acc, byte, index) => {
        if (byte >= 0xE0 && byte <= 0xEF) {
          acc.push(index);
        }
        return acc;
      }, []);

      const analogValues = indexes.reduce<AnalogData[]>((acc, index) => {
        const valueBytes = values.slice(index + 1, index + 3);

        const analogNumber = values[index] - 0xE0;
        const value = significantBytesToNumber(valueBytes);

        acc.push({
          analogNumber, value,
        });
        return acc;
      }, []);

      return analogValues;
    },
  },
]