import { PinMode } from "./firmata-utils";

/** 命令 Key 種類 */
export enum CmdKey {
  QUERY_CAPABILITY = 'query-capability',
  QUERY_ANALOG_MAPPING = 'query-analog-mapping',
  SET_MODE = 'set-mode',
  SET_DIGITAL_PIN_VALUE = 'set-digital-pin-value',
}

interface CmdDefine<T extends `${CmdKey}`, P = undefined> {
  /** 命令 key */
  key: T;
  /** 取得命令資料 */
  getValue: (params: P) => number[];
}

export type FirmataCmdParams = Parameters<FirmataCmd['getValue']>[0];

/** 查詢所有腳位與功能命令 */
type CmdQueryCapability = CmdDefine<'query-capability'>;
/** 查詢類比腳位映射命令 */
type CmdQueryAnalogMapping = CmdDefine<'query-analog-mapping'>;
/** 設定腳位模式 */
type CmdSetMode = CmdDefine<'set-mode', {
  pin: number;
  mode: PinMode;
}>;
/** 設定腳位模式 */
type CmdSetDigitalPinValue = CmdDefine<'set-digital-pin-value', {
  pin: number;
  value: boolean;
}>;

export type FirmataCmd = CmdQueryCapability | CmdQueryAnalogMapping | CmdSetMode
  | CmdSetDigitalPinValue;

export const cmds: FirmataCmd[] = [
  // query-capability: 查詢所有腳位與功能
  {
    key: 'query-capability',
    getValue() {
      return [0xF0, 0x6B, 0xF7];
    },
  },
  // query-analog-mapping: 查詢類比腳位映射
  {
    key: 'query-analog-mapping',
    getValue() {
      return [0xF0, 0x69, 0xF7];
    },
  },
  // set-mode: 設定腳位模式
  {
    key: 'set-mode',
    getValue({ pin, mode }) {
      const cmds = [0xF4, pin, mode]

      // Mode 如果為 Digital Input，加入開啟自動回報命令
      if ([
        PinMode.DIGITAL_INPUT, PinMode.INPUT_PULLUP
      ].includes(mode)) {
        const port = 0xD0 + ((pin / 8) | 0);
        cmds.push(port, 0x01);
      }

      return cmds;
    },
  },
  // set-digital-pin-value: 設定數位腳位數值
  {
    key: 'set-digital-pin-value',
    getValue({ pin, value }) {
      const level = value ? 0x01 : 0x00;
      return [0xF5, pin, level];
    },
  },
]