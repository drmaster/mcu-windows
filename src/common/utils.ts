/** 判斷 Array 是否包含另一特徵 Array */
export function matchFeature(array: number[], feature: number[]) {
  const arrayString = array.join();
  const featureString = feature.join();

  return arrayString.includes(featureString);
}

/** 根據指定元素分割矩陣，separator 不會包含在矩陣中 */
export function arraySplit<T>(array: T[], separator: T) {
  const indexes = indexOfAll(array, separator);

  if (indexes.length === 0) {
    return [array];
  }

  const initArray: T[][] = [];

  const part = array.slice(0, indexes[0]);
  initArray.push(part);

  const result = indexes.reduce((acc, pos, index) => {
    const start = pos;
    const end = indexes?.[index + 1];

    let part: T[];

    // end 不存在表示為最後一個
    if (end === undefined) {
      part = array.slice(start + 1);
    } else {
      part = array.slice(start + 1, end);
    }

    acc.push(part);
    return acc;
  }, initArray);


  return result;
}

/** 取得指定元素在矩陣內所有 index */
export function indexOfAll<T>(array: T[], target: T) {
  return array.reduce<number[]>((acc, el, i) => (el === target ? [...acc, i] : acc), []);
}

/** 延遲指定毫秒 */
export function delay(millisecond: number) {
  return new Promise<void>((resolve) => {
    setTimeout(() => {
      resolve();
    }, millisecond);
  });
}

/** 將有效 Bytes 轉為數值
 * @param bytes 有效位元矩陣。bytes[0] 為 LSB
 * @param bitsNum 每 byte 有效位元數，預設為 7
 */
export function significantBytesToNumber(bytes: number[], bitsNum = 7) {
  const number = bytes.reduce((acc, byte, index) => {
    const mesh = 2 ** bitsNum - 1;

    const validBits = byte & mesh;
    acc += (validBits << (bitsNum * index))

    return acc;
  }, 0);

  return number;
}

/** 取得數值特定 Bit
 * @param number 來源數值
 * @param bitIndex bit Index。從最小位元並以 0 開始
 */
export function getBitWithNumber(number: number, bitIndex: number) {
  const mesh = 1 << bitIndex;
  const value = number & mesh;
  return !!value;
}
