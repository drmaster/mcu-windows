import Phaser from 'phaser';
import { delay } from '../../../common/utils';
import { Weapon } from './group-weapon';

export interface Params {
  x?: number;
  y?: number;
  /** 追擊目標 */
  target: Phaser.Physics.Arcade.Sprite;
  weapon?: Weapon;
}

export class SpriteDog extends Phaser.Physics.Arcade.Sprite {
  private target: Phaser.Physics.Arcade.Sprite;
  private weapon?: Weapon;
  health = 10;

  constructor(scene: Phaser.Scene, params?: Params) {
    const x = params?.x ?? 500;
    const y = params?.y ?? 600;
    const target = params?.target;
    if (!target) {
      throw new Error(`target 必填`);
    }

    super(scene, x, y, 'dog-work');
    scene.add.existing(this);
    scene.physics.add.existing(this);

    this.setScale(0.2)
      .setSize(340, 420)
      .setCollideWorldBounds(true);

    this.play('dog-work');

    this.scene = scene;
    this.target = target;
    this.weapon = params.weapon;

    this.initAutomata();
  }

  /** 扣血 */
  subHealth(val = 1) {
    this.health = Phaser.Math.MinSub(this.health, val, 0);
    this.play('dog-beaten', true);

    if (this.health === 0) {
      this.emit('death');
    }
  }

  private initAutomata() {
    // 隨機發射
    this.scene.time.addEvent({
      delay: 500,
      callbackScope: this,
      repeat: -1,
      callback: async () => {
        await delay(Phaser.Math.Between(0, 200));
        this.fire();
      },
    });

    // 追貓
    this.scene.time.addEvent({
      delay: 500,
      callbackScope: this,
      repeat: -1,
      callback: async () => {
        const vx = (this.target.x - this.x) * 1.5;
        const vy = Phaser.Math.Between(-400, 400);

        this.setVelocity(vx, vy);
      },
    });
  }

  fire() {
    this.weapon?.fire(this.x, this.y, -500);
    this.play('dog-attack', true);
  }

  preUpdate(time: number, delta: number) {
    super.preUpdate(time, delta);

    if (!this.anims.isPlaying) {
      this.play('dog-work');
    }
  }
}