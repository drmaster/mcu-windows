import Phaser from 'phaser';
import { JoyStickGame } from '../../../types/main.type';
import { Weapon } from './group-weapon';

export interface Params {
  x?: number;
  y?: number;
  weapon?: Weapon;
}

export class SpriteCat extends Phaser.Physics.Arcade.Sprite {
  private weapon?: Weapon;
  /** 血量 */
  health = 5;
  /** 最大速度 */
  private readonly velocityMax = 300;

  constructor(scene: Phaser.Scene, params?: Params) {
    const x = params?.x ?? 200;
    const y = params?.y ?? 200;

    super(scene, x, y, 'cat-work');

    // 將人物加入至場景並加入物理系統
    scene.add.existing(this);
    scene.physics.add.existing(this);

    // 設定人物縮放、碰撞箱尺寸、碰撞反彈、世界邊界碰撞
    this.setScale(0.3)
      .setSize(220, 210)
      .setBounce(0.2)
      .setCollideWorldBounds(true);

    // 播放動畫
    this.play('cat-work');

    this.scene = scene;
    this.weapon = params?.weapon;

    const joyStick = (scene.game as JoyStickGame).joyStick;
    if (joyStick) {
      joyStick.on('data', ({ x, y }) => {
        // 將 x、y 數值組合為向量並轉為單位向量。
        const velocityVector = new Phaser.Math.Vector2(x, y);
        velocityVector.normalize();

        // 將單位向量 x、y 分量分別乘上最大速度
        const { x: vx, y: vy } = velocityVector;
        this.setVelocity(vx * this.velocityMax, vy * this.velocityMax);
      })

      joyStick.on('rising', () => {
        // 座標設為與主角相同位置
        this.weapon?.fire(this.x, this.y, 800);

        // 播放主角發射動畫
        this.play('cat-attack', true);
        this.setVelocity(0, 0);
      });

      this.once('destroy', () => {
        joyStick.removeAllListeners();
      });
    }
  }

  /** 扣血 */
  subHealth(val = 1) {
    this.health = Phaser.Math.MinSub(this.health, val, 0);
    this.play('cat-beaten', true);

    // 生命值為 0 時，發出 death 事件
    if (this.health === 0) {
      this.emit('death');
    }
  }

  preUpdate(time: number, delta: number) {
    super.preUpdate(time, delta);

    // 沒有任何動畫播放時，播放 cat-work
    if (!this.anims.isPlaying) {
      this.play('cat-work');
    }
  }
}