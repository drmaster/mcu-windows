import Phaser from 'phaser';

/** 規定武器一定要含有 fire method */
export interface Weapon {
  fire: (x: number, y: number, velocity: number) => void
}

type WeaponClass = new (...args: any[]) => Weapon;

export interface Params<T> {
  /** 注入武器物件 */
  classType: T;
  key: string;
  /** 物體數量 */
  quantity: number;
}

export class GroupWeapon<T extends WeaponClass> extends Phaser.Physics.Arcade.Group {
  constructor(scene: Phaser.Scene, params: Params<T>) {
    super(scene.physics.world, scene);

    const { classType, key, quantity = 5 } = params;

    // 建立多個內容
    this.createMultiple({
      classType,
      frameQuantity: quantity,
      active: false,
      visible: false,
      key,
    });

    /**
     * 物件預設 depth 為 0，
     * 設定為 1 就可以在最上層了
     */
    this.setDepth(1);

    // 移到場景外，隱藏武器
    this.setXY(-1000, -1000);
  }

  /** 發射武器 */
  fire(x: number, y: number, velocity: number) {
    // 取得群組中被停用的一個物件
    const weapon = this.getFirstDead(false);

    // 若存在則呼叫 fire method
    if (weapon) {
      weapon.body.enable = true;
      weapon.fire(x, y, velocity);
    }
  }
}