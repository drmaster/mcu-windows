import Phaser from 'phaser';
import { Weapon } from './group-weapon';

export interface Params {
  x?: number;
  y?: number;
}

export class SpriteWeaponDog extends Phaser.Physics.Arcade.Sprite implements Weapon {
  constructor(scene: Phaser.Scene, params: Params) {
    const { x = 0, y = 0 } = params;

    super(scene, x, y, 'dog-weapon');
    this.scene = scene;
  }

  /** 此 method 會被不停呼叫，可以用於持續判斷條件 */
  preUpdate(time: number, delta: number) {
    super.preUpdate(time, delta);

    /** 檢查武器是否超出世界邊界
     * 透過偵測武器是否與世界有碰撞，取反向邏輯
     * 沒有碰撞，表示物體已經超出邊界
     */
    const outOfBoundary = !Phaser.Geom.Rectangle.Overlaps(
      this.scene.physics.world.bounds,
      this.getBounds(),
    );

    // 隱藏超出邊界武器並關閉活動
    if (outOfBoundary) {
      this.setActive(false)
        .setVisible(false);
    }
  }

  /** 發射武器 */
  fire(x: number, y: number, velocity: number) {
    // 清除所有加速度、速度並設置於指定座標
    this.body.reset(x, y);

    // 角速度
    const angularVelocity = Phaser.Math.Between(-400, 400);

    this.setScale(0.2)
      .setSize(300, 300)
      .setAngularVelocity(angularVelocity)
      .setVelocityY(velocity)
      .setActive(true)
      .setVisible(true);
  }
}