import Phaser from 'phaser';
import { JoyStickGame } from '../../../types/main.type';

export interface GameResult {
  result: 'win' | 'lose';
}

export default class extends Phaser.Scene {
  declare game: JoyStickGame;

  constructor() {
    super({ key: 'over' })
  }
  create({ result }: GameResult) {
    const x = Number(this.game.config.width) / 2;
    const y = Number(this.game.config.height) / 2;

    const text = result === 'win' ? '恭喜獲勝' : '哭哭，被打敗了';
    const texture = result === 'win' ? 'cat-attack' : 'cat-beaten';

    // 主角
    const cat = this.physics.add.sprite(x, y - 80, texture)
      .setScale(0.5);

    // 提示文字
    this.add.text(x, y + 50, text, {
      color: '#000',
      fontSize: '30px',
    }).setOrigin(0.5);

    this.add.text(x, y + 100, '按下搖桿按鍵重新開始', {
      color: '#000',
      fontSize: '18px',
    }).setOrigin(0.5);

    const joyStick = this.game.joyStick
    if (joyStick) {
      // 延遲一秒鐘後再偵測搖桿按鈕，防止一進到場景後誤按按鈕馬上觸發
      setTimeout(() => {
        joyStick.once('toggle', () => {
          this.scene.start('main');
        });
      }, 1000);
    }
  }
}