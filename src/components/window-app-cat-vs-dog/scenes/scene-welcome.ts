import Phaser from 'phaser';
import { JoyStickGame } from '../../../types/main.type';

export default class extends Phaser.Scene {
  private cat!: Phaser.Types.Physics.Arcade.SpriteWithDynamicBody;
  declare game: JoyStickGame;

  constructor() {
    super({ key: 'welcome' })
  }
  create() {
    const x = Number(this.game.config.width) / 2;
    const y = Number(this.game.config.height) / 2;

    this.cat = this.physics.add
      .sprite(x, y - 80, 'cat-work')
      .setScale(0.5)
      .setCollideWorldBounds(true);

    this.cat.anims.play('cat-work');

    this.add.text(x, y + 50, '按下搖桿按鍵開始', {
      color: '#000',
      fontSize: '30px',
    }).setOrigin(0.5);

    const joyStick = this.game.joyStick;
    if (!joyStick) {
      console.error(`joyStick 不存在`);
      return;
    }

    joyStick.on('data', ({ x, y }) => {
      this.cat.setVelocity(x, y);
    })

    joyStick.once('toggle', () => {
      // 前往 main 場景
      this.scene.start('main');
    });

    /** 監聽 destroy 事件，清除所有搖桿監聽器
     * 以免場景切換後，搖桿還持續呼叫人物的 setVelocity，導致錯誤
     */
    this.cat.once('destroy', () => {
      joyStick.removeAllListeners();
    });
  }
}