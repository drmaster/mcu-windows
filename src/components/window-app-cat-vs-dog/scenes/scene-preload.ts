import Phaser from 'phaser';

import catWeapon from '../../../assets/cat-vs-dog/cat-weapon.png';
import dogWeapon from '../../../assets/cat-vs-dog/dog-weapon.png';
import river from '../../../assets/cat-vs-dog/river.png';

import catWork from '../../../assets/cat-vs-dog/cat-work.png';
import catBeaten from '../../../assets/cat-vs-dog/cat-beaten.png';
import catAttack from '../../../assets/cat-vs-dog/cat-attack.png';

import dogWork from '../../../assets/cat-vs-dog/dog-work.png';
import dogBeaten from '../../../assets/cat-vs-dog/dog-beaten.png';
import dogAttack from '../../../assets/cat-vs-dog/dog-attack.png';

export default class extends Phaser.Scene {
  constructor() {
    super({ key: 'preload' })
  }
  preload() {
    // 載入圖片
    const imgs = {
      'cat-weapon': catWeapon,
      'dog-weapon': dogWeapon,
      'river': river,
    };

    Object.entries(imgs).forEach(([key, value]) => {
      this.load.image(key, value);
    });

    // 載入 spritesheet
    const catSheets = {
      'cat-work': catWork,
      'cat-beaten': catBeaten,
      'cat-attack': catAttack,
    };
    const dogSheets = {
      'dog-work': dogWork,
      'dog-beaten': dogBeaten,
      'dog-attack': dogAttack,
    };

    Object.entries(catSheets).forEach(([key, value]) => {
      this.load.spritesheet(key, value,
        { frameWidth: 300, frameHeight: 300 }
      );
    });
    Object.entries(dogSheets).forEach(([key, value]) => {
      this.load.spritesheet(key, value,
        { frameWidth: 450, frameHeight: 450 }
      );
    });
  }
  create() {
    // 建立動畫
    this.anims.create({
      key: 'cat-work',
      frames: this.anims.generateFrameNumbers('cat-work', { start: 0, end: 1 }),
      frameRate: 4,
      repeat: -1,
    });
    this.anims.create({
      key: 'cat-attack',
      frames: this.anims.generateFrameNumbers('cat-attack', { start: 0, end: 0 }),
      frameRate: 4,
      repeat: 1,
    });
    this.anims.create({
      key: 'cat-beaten',
      frames: this.anims.generateFrameNumbers('cat-beaten', { start: 0, end: 0 }),
      frameRate: 4,
      repeat: 1,
    });

    this.anims.create({
      key: 'dog-work',
      frames: this.anims.generateFrameNumbers('dog-work', { start: 0, end: 1 }),
      frameRate: 4,
      repeat: -1,
    });
    this.anims.create({
      key: 'dog-attack',
      frames: this.anims.generateFrameNumbers('dog-attack', { start: 0, end: 0 }),
      frameRate: 4,
      repeat: 1,
    });
    this.anims.create({
      key: 'dog-beaten',
      frames: this.anims.generateFrameNumbers('dog-beaten', { start: 0, end: 0 }),
      frameRate: 4,
      repeat: 1,
    });

    // 前往下一個場景
    this.scene.start('welcome');
  }
}