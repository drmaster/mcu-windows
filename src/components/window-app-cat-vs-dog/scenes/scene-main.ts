import Phaser from 'phaser';
import { JoyStickGame } from '../../../types/main.type';
import { SpriteCat } from '../objects/sprite-cat';
import { SpriteDog } from '../objects/sprite-dog';
import { GroupWeapon } from '../objects/group-weapon';
import { SpriteWeaponCat } from '../objects/sprite-weapon-cat';
import { SpriteWeaponDog } from '../objects/sprite-weapon-dog';

export default class extends Phaser.Scene {
  private platforms!: Phaser.Physics.Arcade.StaticGroup;
  declare game: JoyStickGame;

  private cat!: SpriteCat;
  private dog!: SpriteDog;

  private catHealthText!: Phaser.GameObjects.Text;
  private dogHealthText!: Phaser.GameObjects.Text;

  constructor() {
    super({ key: 'main' })
  }
  create() {
    // 加入中央河流
    this.platforms = this.physics.add.staticGroup();
    this.platforms.create(300, 400, 'river').setScale(0.17).refreshBody();

    // 建立主角
    const catWeapon = new GroupWeapon(this, {
      classType: SpriteWeaponCat,
      key: 'cat-weapon',
      quantity: 1
    });
    this.cat = new SpriteCat(this, {
      weapon: catWeapon
    });

    // 建立敵人
    const dogWeapon = new GroupWeapon(this, {
      classType: SpriteWeaponDog,
      key: 'dog-weapon',
      quantity: 5
    });
    this.dog = new SpriteDog(this, {
      target: this.cat,
      weapon: dogWeapon
    });

    // 加入河流與人物碰撞
    this.physics.add.collider([this.cat, this.dog], this.platforms);


    // 顯示生命值
    this.catHealthText = this.add.text(20, 20, `貓命：${this.cat.health}`, {
      color: '#000',
      fontSize: '14px',
    });

    const sceneHeight = Number(this.game.config.height);
    this.dogHealthText = this.add.text(20, sceneHeight - 20, `狗血：${this.dog.health}`, {
      color: '#000',
      fontSize: '14px',
    }).setOrigin(0, 1);


    // 加入武器與人物碰撞
    this.physics.add.overlap(this.cat, dogWeapon, (cat, weapon: any) => {
      /**
       * weapon 回傳型別有誤，所以只好先設為 any
       */

      // 隱藏武器;
      weapon.body.enable = false;
      weapon.setActive(false).setVisible(false);

      // 主角扣血
      this.cat.subHealth();
    });

    this.physics.add.overlap(this.dog, catWeapon, (dog, weapon: any) => {
      // 隱藏武器
      weapon.body.enable = false;
      weapon.setActive(false).setVisible(false);

      // 敵人扣血
      this.dog.subHealth();
    });

    // 偵測人物事件，跳轉並傳遞結果
    this.dog.once('death', () => {
      this.scene.start('over', { result: 'win' });
    });
    this.cat.once('death', () => {
      this.scene.start('over', { result: 'lose' });
    });
  }
  update() {
    this.catHealthText.setText(`貓命：${this.cat.health}`);
    this.dogHealthText.setText(`狗血：${this.dog.health}`);
  }
}