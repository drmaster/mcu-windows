# mcu-windows


本系列文已改編成書[「Arduino 自造趣：結合 JavaScript x Vue x Phaser 輕鬆打造個人遊戲機」](https://www.tenlong.com.tw/products/9786263333130)，本書改用 Vue3 與 TypeScript 全面重構且加上更詳細的說明，

在此感謝 iT 邦幫忙、博碩文化與編輯小 p 的協助，歡迎大家前往購書，鱈魚在此感謝大家 (｡･∀･)。

若想 DIY 卻不知道零件去哪裡買的讀者，可以參考此[連結](https://www.ruten.com.tw/item/show?22245679885806) 。( •̀ ω •́ )✧
